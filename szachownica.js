let chess = new Chess();
let figury = [];
let szachownica = document.getElementById("szachownica")
let gra;
let rozmiarPola = 800;

window.onload = () => {
    if (window.innerHeight > window.innerWidth){
        // wersja mobilna
        szachownica.style.width  = Math.floor(window.innerWidth*0.9)+"px";
        szachownica.style.height = Math.floor(window.innerWidth*0.9)+"px";

        szachownica.style.backgroundSize = szachownica.style.width+" "+szachownica.style.height;
    }
    else{
        szachownica.style.width  = "800px";
        szachownica.style.height = "800px";
    }

    rozmiarPola = szachownica.clientWidth/8;
    gra = new Gra()
};

let xy2pgn = (x = -1, y = -1) => {
    if ( ((x < 0) || (y < 0)) || ((x > 8) || (y > 8)) ) return "Error";
    else return String.fromCharCode(x+97) + String.fromCharCode(56-y);
}

let pgn2xy = (pgn) => {
    if(pgn == "O-O" || pgn == "O-O-O") return pgn;
    else pgn = pgn.match(/[abcdefgh][12345678]/)[0];

    x = pgn.charCodeAt(0)-97;
    y = 8-pgn[1];

    if ( ((x < 0) || (y < 0)) || ((x > 8) || (y > 8)) ) return "Error";
    else return { x: x, y: y };
}

let figuraPodXY = (x,y) =>{
    for (let fig of szachownica.children){
        if ( (fig.style.left == x*rozmiarPola+"px") && (fig.style.top == y*rozmiarPola+"px") ){
            return fig;
        }
    }
}

class Gra{
    constructor(){
        chess = new Chess();
        this.board = chess.board();

        // przejdz przez wszystkie figury i je utwórz
        for (let row in this.board){
            for (let column in this.board[row]){

                let figura = this.board[row][column];

                if (figura != null){
                    new Figura(figura.type + figura.color, Number(column), Number(row));
                }
            }
        }
    }
}

class Figura{
    constructor(type, x, y){
        this.type = type;
        this.x = x;
        this.y = y;
        this.pokazywane = false;

        this.img = new Image();
        this.img.src = "figury/"+type+".svg";

        this.img.style.width = rozmiarPola+"px";
        this.img.style.height = rozmiarPola+"px";

        this.img.style.left = this.x*rozmiarPola+"px";
        this.img.style.top =  this.y*rozmiarPola+"px";
        
        szachownica.appendChild(this.img);
        figury.push(this);
    }
    
    rusz(dx,dy){
        let res = chess.move({from: xy2pgn(this.x, this.y), to: xy2pgn(dx,dy)});
        if (res != null){
            this.ukryjMozliweRuchy();
            let df = figuraPodXY(dx,dy);

            if (res.flags == "c" || res.flags == "e"){
                if (res.flags == "e"){
                    // en passant
                    // jeżeli białego ruch to usuń pionka (y+1)
                    // jeżeli czarnego ruch to usuń pionka (y-1)

                    if (chess.turn() == "b") df = figuraPodXY(dx,dy+1);
                    else df = figuraPodXY(dx,dy-1);
                }

                df.style.left = "1000px";
                df.style.top = "400px";
                df.style.display = "none";
            }
            else if (res.flags == "k"){
                // roszada krótka
                // weź wieże (x+3 od tej pozycji) i zmień jej pozycje na x-2 
                let n = Array.from(szachownica.children).indexOf(this.img)+3;

                figury[n].x -= 2;
                figury[n].img.style.left = figury[n].x*rozmiarPola + "px";
            }

            else if (res.flags == "q"){
                // roszada długa
                // weź wieże (x-4 od tej pozycji) i zmień jej pozycje na x+3
                let n = Array.from(szachownica.children).indexOf(this.img)-4;

                figury[n].x += 3;
                figury[n].img.style.left = figury[n].x*rozmiarPola + "px";
            }

            this.x = dx;
            this.y = dy;
        }
        this.img.style.left = this.x*rozmiarPola+"px"
        this.img.style.top =  this.y*rozmiarPola+"px"
    } 

    pokazMozliweRuchy(){
        if (!this.pokazywane){
            this.pokazywane = true;

            for (let move of chess.moves({square: xy2pgn(this.x, this.y)}) ){
                let img = new Image();
                let movePGN = {};

                img.src = "figury/active.svg"

                img.style.width  = rozmiarPola+"px";
                img.style.height = rozmiarPola+"px";

                if (move == "O-O" ){
                    if (chess.turn() == "w") movePGN = {x: 6, y: 7};
                    else movePGN = {x: 6, y: 0};
                }
                else if(move == "O-O-O"){
                    if (chess.turn() == "w") movePGN = {x: 2, y: 7};
                    else movePGN = {x: 2, y: 0};
                }
                else{
                    movePGN = pgn2xy(move);
                }

                img.style.left = movePGN.x*rozmiarPola+"px"
                img.style.top  = movePGN.y*rozmiarPola+"px"

                img.classList = "active";
        
                szachownica.appendChild(img);

            }
        }
    }

    ukryjMozliweRuchy(){
        this.pokazywane = false;
        for ( let el of Array.from(document.getElementsByClassName("active")) ){
            el.remove();
        }
    }
}
let aktywny = {};
let mousedown = false

szachownica.addEventListener("mousedown", (e) => {
    mousedown = true;

    let dx = e.clientX-szachownica.offsetLeft;
    let dy = e.clientY-szachownica.offsetTop;

    if( (e.target != szachownica) && (!e.target.classList.contains("active")) ){
        e.preventDefault();

        // ukryj ostatnio wyświetlane możliwe ruchy jeżeli są inne od obecnych
        if (Object.keys(aktywny).length > 0){
            if(aktywny != figury[Array.from(szachownica.children).indexOf(e.target)]){
                aktywny.ukryjMozliweRuchy();
            }
        } 

        aktywny = figury[Array.from(szachownica.children).indexOf(e.target)];
        aktywny.img.style.zIndex = "100";

        aktywny.img.style.left = dx-rozmiarPola/2+"px";
        aktywny.img.style.top  = dy-rozmiarPola/2+"px";

        aktywny.pokazMozliweRuchy();
    }
    else if( (e.target.classList.contains("active")) && (Object.keys(aktywny).length > 0) ){
        e.preventDefault();

        aktywny.rusz(Math.floor(dx/100), Math.floor(dy/100));
        aktywny = {}
    }
});

szachownica.addEventListener("mousemove", (e) => {
    if (mousedown && Object.keys(aktywny).length > 0){
        e.preventDefault();
        aktywny.img.style.left = e.clientX-szachownica.offsetLeft-(rozmiarPola/2) + "px";
        aktywny.img.style.top  = e.clientY-szachownica.offsetTop-(rozmiarPola/2) + "px";
    }
});

szachownica.addEventListener("mouseup", (e) => {
    if((Object.keys(aktywny).length > 0)){
        aktywny.img.style.zIndex = "1";

        let dx = Math.floor((e.clientX-szachownica.offsetLeft)/rozmiarPola); 
        let dy = Math.floor((e.clientY-szachownica.offsetTop)/rozmiarPola); 

        aktywny.rusz(dx, dy);

        if( dx != aktywny.x || dy != aktywny.y ){
            aktywny.ukryjMozliweRuchy();
            aktywny = {}
        }
    }
    mousedown = false;
});